require 'open-uri'
require 'net/http'
require 'date'
require 'zlib' 

def get_content_length(test_url)
  headers = { "accept-encoding" => "gzip;q=1.0,deflate;q=0.6,identity;q=0.3" }
  url = URI.parse test_url
  res = Net::HTTP.start(url.host, url.port) {|http|
    http.get(test_url, headers)
  }
  headers = res.to_hash
  gzipped = headers['content-encoding'] && headers['content-encoding'][0] == "gzip"
  content = gzipped ? Zlib::GzipReader.new(StringIO.new(res.body)).read : res.body 

  full_length = content.length
  compressed_length = (headers["content-length"] && headers["content-length"][0] || res.body.length)
  return [full_length.to_i,compressed_length.to_i]
end

def test_speed(url)
  test_start_time = Time.now
  full_length, compressed_length = get_content_length(url)
  test_end_time = Time.now
  
  puts "\r\n---output ---"
  puts "test_start_time: #{test_start_time}"
  puts "full_length=#{full_length} bytes ; compressed_length=#{compressed_length} bytes"
  puts "test_end_time: #{test_start_time}"
  
  spend_time = (test_end_time - test_start_time)
  
  puts "\r\nspeed time: #{spend_time}s"
  puts "speed: #{compressed_length/spend_time/8/1024} KB/s"
end

test_url  = "http://s1.eoe.cn/www/default/common/images/logo/logo.png"
test_speed(test_url)


=begin
---output ---
test_start_time: Sat Mar 09 21:27:31 +0800 2013
full_length=5048 bytes ; compressed_length=5048 bytes
test_end_time: Sat Mar 09 21:27:31 +0800 2013

speed time: 0.054595s
speed: 11.286948209543 KB/s
=end


